import java.util.Scanner;
import java.util.Random;

public class TicTacToeApp{
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		int row; int col;
		boolean notContinue = false;
		int player1Won = 0;
		int player2Won = 0;
		
		while (!notContinue) {
			System.out.println("--------------------------------");
			System.out.println("Welcome to the Tic Tac Toe game!" + "\n" + 
			"Player 1's token: X" + "\n" + "Player 2's token: O");
			Board b = new Board(); //generate a new board every time we restart
			boolean gameOver = false;
			int player = 1;
			Tile playerToken = Tile.X;
			
			//start the game
			while (!gameOver){
				System.out.println("\n" + b.toString() + "\n");
				System.out.println("Player " + player + ": it's your turn where do you want to place your token?");
				System.out.print("row: ");
				row = reader.nextInt();
				System.out.print("column: ");
				col = reader.nextInt();
				
				//check which player's turn it is
				if (player == 1) 
					playerToken = Tile.X;
				else
					playerToken = Tile.O;
				
				//check if player has successfully placed their token
				while (!b.placeToken(row, col, playerToken)){
					System.out.println("Please re-enter the row and column!");
					System.out.print("row: ");
					row = reader.nextInt();
					System.out.print("column: ");
					col = reader.nextInt();
				}
				
				//check if there's a winner or if the board is full
				if (b.checkIfWinning(playerToken)){
					System.out.println("\n" + b.toString() + "\n");
					System.out.println("Player " + player + " wins!");
					gameOver = true;
					//bonus part:
					if (player == 1)
						player1Won ++;
					else
						player2Won ++;
					System.out.println("-----" + "\n" + 
									   "Player 1 has won " + player1Won + " game(s)" + "\n" +
									   "Player 2 has won " + player2Won + " game(s)");
					System.out.println("Continue? Type Yes/No");
					if (reader.next().toLowerCase().equals("no")){
						notContinue = true;
					}
				} else if (b.checkIfFull()){
					System.out.println("\n" + b.toString() + "\n");
					System.out.println("It's a tie!");
					gameOver = true;
					//bonus part:
					System.out.println("-----" + "\n" + 
									   "Player 1 has won " + player1Won + " game(s)" + "\n" +
									   "Player 2 has won " + player2Won + " game(s)");
					System.out.println("Continue? Type Yes/No");
					if (reader.next().toLowerCase().equals("no")){
						notContinue = true;
					}
				} else {
					player ++;
					if (player > 2)
						player = 1;
				}
			}
		}
	}
}