public class Board{
	//fields
	private Tile[][] grid;
	final int rows = 3;
	final int columns = 3;
	//constructor
	public Board(){
		grid = new Tile[rows][columns];
		//initialize each Tile inside grid to "_"
		for(int r=0; r<grid.length; r++){
			for(int col=0; col<grid[r].length; col++)
				grid[r][col] = Tile.BLANK;
		}
	}
	//methods
	public String toString(){
		String result = "   0  1  2";
		for(int r=0; r<grid.length; r++){
			result += "\n" + r + "  ";
			for(int col=0; col<grid[r].length; col++)
				result += grid[r][col].getName() + "  "; 
		}
		return result;
	}
	/*method check if given row and col is valid to play
	* if tile == BLANK --> assign + return true
	* if tile != BLANK --> return false
	*/
	public boolean placeToken(int row, int col, Tile playerToken){
		//check if row && col are in correct range 
		if (row<this.rows && col<this.columns){
			//check if a tile is equal to BLANK
			if (grid[row][col] == Tile.BLANK){
				grid[row][col] = playerToken;
				return true;
			} else 
				return false;
		} else
			return false;
	}
	/*method takes as no inputs 
	* returns false if the Tile[][] has at least 1 BLANK value
	*/
	public boolean checkIfFull(){
		int num=0; //number of BLANK in the board
		for(int r=0; r<grid.length; r++){
			for(int col=0; col<grid[r].length; col++){
				if(grid[r][col] == Tile.BLANK)
					num++;
			}
		}		
		return !(num >= 1); 
	}
	/*method check if all tiles in a single row of Tile[][] is equal to the playerToken
	* true --> player win
	* false --> not yet
	*/
	private boolean checkIfWinningHorizontal(Tile playerToken){
		boolean isWinning = true;
		for (int r=0; r<grid.length; r++){
			isWinning = true; //assume that all tiles in a single row are equal to playerToken
			for(int col=0; col<grid[r].length; col++){
				if(grid[r][col] != playerToken)
					isWinning = false;
			}
			if(isWinning) //if true, meaning every tile is equal to playerToken horizontally
				return true;
		}
		return isWinning;
	}
	/*method check if all tiles in one column are equal to playerToken
	*/
	private boolean checkIfWinningVertical(Tile playerToken){
		/*boolean isWinning = true;
		int col = 0;
		while (col < this.columns){
			isWinning = true; //assume that all tiles in the column are equal to playerToken
			for (int i=0; i<grid.length; i++){
				if(grid[i][col] != playerToken)
					isWinning = false;
			}
			if(isWinning) //if true, meaning that the assumption at the beginning before loop this row is right --> every tile is equal to playerToken
				return true;
			col++;
		}
		return isWinning;*/
		//
		boolean isWinning = true;
		for (int col=0; col<grid.length; col++){
			isWinning = true; //assume that all tiles in a the column are equal to playerToken
			for(int r=0; r<grid.length; r++){
				if(grid[r][col] != playerToken)
					isWinning = false;
			}
			if(isWinning) //if true, meaning every tile is equal to playerToken vertically
				return true;
		}
		return isWinning;
	}
	/*method check if all tiles in one of the 2 diagonals are equal to playerToken
	*/
	private boolean checkIfWinningDiagonal(Tile playerToken){
		boolean isWinning = true; 
		int col = 0;
		//check 1st diagonal (left to right - downward)
		while (col<grid.length){
			isWinning = true; //assume that all tiles in this diagonal are equal to playerToken
			for (int r=0; r<grid.length; r++){
				if(grid[r][col] != playerToken)
					isWinning = false;
				col++;
			}
			if(isWinning) //if true, meaning every tile is equal to playerToken diagonally
				return true;		
		}
		//these code below only run when player haven't won the 1st diagonal
		//now, check 2nd diagonal (left to right - upward)
		col = 0; 
		while (col<grid.length){
			isWinning = true; //assume that all tiles in this diagonal are equal to playerToken
			for (int r=grid.length-1; r>=0; r--){
				if(grid[r][col] != playerToken)
					isWinning = false;
				col++;
			}		
		}
		return isWinning; 
	}
	/*method uses 3 private methods above to check if one of them is true 
	* return true if one of them is true
	*/
	public boolean checkIfWinning(Tile playerToken){
		return (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken));
	}
}