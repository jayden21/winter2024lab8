enum Tile{
	BLANK("_"),
	X("X"),
	O("O");
	
	final String name;
	//constructor
	private Tile(String name){
		this.name = name;
	}
	//getter
	public String getName(){
		return this.name;
	}
}

